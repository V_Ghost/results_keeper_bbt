class WebApp

  def method_missing(name)
    klass_name = convert_to_class(name)
    Object.const_defined?(klass_name) ? Object.const_get(klass_name).new : raise("#{klass_name} page is not defined")
  end

  private

  def convert_to_class(name)
    name.to_s.split('_').collect(&:capitalize).join
  end
end
