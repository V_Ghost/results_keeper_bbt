require_relative 'base_page'
require_relative '../sections/user_top_section'

class ProjectsPage < BasePage

  element :new_project, '.create-new-project'
  elements :project_items, '.project-item'
  element :revisions_count, '.project-revision:contains("2")'
  sections :top_section, UserTopSection, '.header-main'

  def delete_project(project_name)
    project_by_name(project_name).find('.delete-project').click
    accept_alert
  end

  def project_by_name(project_name)
    projects_by_name(project_name).first
  end

  def projects_by_name(project_name)
    project_items.select {|x| x.text.include?(project_name)}
  end

  def click_project(project_name)
    project_by_name(project_name).click
  end

  def edit_project(project_name)
    project_by_name(project_name).find('.edit-project').click
  end

  def click_new_project
    new_project.click
  end

  def project_exists?(project_name)
    projects_by_name(project_name).size > 0
  end

  def revisions_count(project_name)
    project_by_name(project_name).find('.project-revision').text
  end

  def remove_all_projects
    puts '=== REMOVING PROJECTS ==='

    all('.project-item').count.times do
      first('.delete-project').click
      accept_alert
    end
  end

end
