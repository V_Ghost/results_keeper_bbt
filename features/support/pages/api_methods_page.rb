class ApiMethodsPage < SitePrism::Page

  def hash_values_to_string(hash)
    hash.keys.each {|k| hash[k] = hash[k].to_s}
    hash
  end

  def hash_value_to_string(array)
    array.each {|hash| hash_values_to_string(hash) }
  end

  def all_hashes_present?(response, table)
    num = table.count
    counter = 0
    table.each do |table_hash|
      hash_value_to_string(response).each do |hash|
        if hash >= table_hash
          counter += 1
        end
      end
    end
    return num == counter
  end
end
