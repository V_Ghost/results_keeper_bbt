class RegisterPage < BasePage

  element :first_name_input, '#user_first_name'
  element :last_name_input, '#user_last_name'
  element :email_input, '#user_email'
  element :password_input, '#user_password'
  element :pasword_confirm_input, '#user_password_confirmation'
  element :submit_button, '.btn.auth-button'

  def fill_register_data (first_name, last_name, email, password)
    first_name_input.set(first_name)
    last_name_input.set(last_name)
    email_input.set(email)
    password_input.set(password)
    pasword_confirm_input.set(password)
  end

  def click_submit_button
    submit_button.click
  end
end
