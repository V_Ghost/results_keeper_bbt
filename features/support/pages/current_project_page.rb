class CurrentProjectPage < BasePage

  element :revision_analytics, "a[href*='/analytics'"
  element :error_trend_tab, "a[href*='#error-trend']"
  element :return_all_failures_tab, "a[href*='#run-all-failures']"
  element :tests_amount, '.column-tests-amount'

  def click_revision_analytics
    revision_analytics.click
  end

  def click_current_revision(revision_name)
    find('a', :text => revision_name).click
  end

  def click_error_trend_tab
    error_trend_tab.click
  end

  def click_return_all_failures_tab
    return_all_failures_tab.click
  end

end
