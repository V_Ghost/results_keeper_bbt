require_relative 'base_page'
require 'site_prism/table'

class RevisionsPage < BasePage
  extend SitePrism::Table

  element :name_project_link, '.breadcrumbs-item active'
  element :project_analitics_link, :link_text, 'Project Analytics'

  element :revision_amount, '.revision-amount.summary span'
  element :revision_passed, '.revision-passed.summary span'
  element :revision_failed, '.revision-failed.summary span'

  table :revisions_table, 'tbody' do
    column :name
    column :status
    column :tests_amount
    column :failed_amount
    column :duration
    column :started_at
  end

  def revision_exists?(revision_name)
    revisions_table.rows.select {|row| row[:name] == revision_name}.size > 0
  end

  def row(revision_name)
    revisions_table.rows.find {|row| row[:name] == revision_name}
  end

  def duration(revision_name)
    row(revision_name)[:duration]
  end

  def duration_in_seconds(revision_name)
    duration(revision_name).gsub('00:', '').to_i
  end

  def tests_amount(revision_name)
    row(revision_name)[:tests_amount]
  end

  def status(revision_name)
    row(revision_name)[:status]
  end

  def started_at(revision_name)
    row(revision_name)[:started_at]
  end

  def failed_amount(revision_name)
    row(revision_name)[:failed_amount]
  end

  def name(revision_name)
    row(revision_name)[:name]
  end

  def click_delete_revision(revision_name)
    delete_button = find "a[title*='Remove #{revision_name} revision.']"
    delete_button.click
    page.driver.browser.switch_to.alert.accept
  end

  def click_finish_revision(revision_name)
    finish_button = find "a[title*='Finish #{revision_name} revision.']"
    finish_button.click
    page.driver.browser.switch_to.alert.accept
  end

  def revision_amount_text
    revision_amount.text
  end

  def revision_passed_text
    revision_passed.text
  end

  def revision_failed_text
    revision_failed.text
  end
end
