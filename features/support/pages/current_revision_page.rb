class CurrentRevisionPage < BasePage

    def click_current_test(test_name)
      find('a', :text => test_name).click
    end
end
