require_relative '../pages/base_page'
require_relative '../sections/user_top_section'

class UserPage < BasePage

  element :user_name_label, '.user-name'
  element :email_label, '.user-email'

  element :copy_button, '.fa fa-clipboard'
  element :new_key_link, '.user-key-generation'
  element :secret_key_field, '#user-secret-key'

  sections :top_section, UserTopSection, '.header-main'

  def genertate_new_secret_key
    new_key_link.click
    click_link 'Generate New Secret Key'
  end
end
