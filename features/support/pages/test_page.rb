class TestPage < BasePage

  element :location, '.test-screen-link'
  element :run_amount_passed, '.run-amount-passed'
  element :run_amount_failed, '.run-amount-failed'

  def location_text
    location.text
  end

  def run_amount_passed_text
    run_amount_passed.text
  end

  def run_amount_failed_text
    run_amount_failed.text
  end

end
