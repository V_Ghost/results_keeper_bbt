require_relative 'base_page'

class LoginPage < BasePage

  element :email_input, '#user_email'
  element :password_input, '#user_password'
  element :login_button, '#submit-button'
  element :forgot_password_link, 'a', text: 'Forgot your password?'
  element :confirmation_instructions_link, 'a', text: 'Didn\'t receive confirmation instructions?'
  element :register_link, '*[href= "/users/sign_up"]'
  element :forgot_password_link, 'a', text: 'Forgot your password?'
  element :confirmation_instructions_link, 'a', text: 'Didn\'t receive confirmation instructions?'

  def type_creds(email, password)
    email_input.set(email)
    password_input.set(password)
  end

  def click_login
    login_button.click
  end

  def click_register
    register_link.click
  end
end
