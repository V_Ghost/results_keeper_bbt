class GmailPage < BasePage

  def login(username, password)
    Gmail.connect(username, password)
  end

  def find_email(subject)
    sleep CommonVars::DEFAULT_DELAY until
        message = login(CommonVars:: EMAIL_USER_LOGIN, CommonVars::EMAIL_USER_PASSWORD)
                                        .inbox.emails(:subject => subject).first
    message
  end

  def verification_link(subject)
    /<a.*?href="(.+?)".*?>Confirm my account<\/a>/m
        .match(find_email(subject).body.decoded)[1].gsub(/\s/, '')
        .gsub('localhost', CommonVars::API_SERVER_HOST)
  end

  def delete_email(subject)
    find_email(subject).delete!
  end

  def visit_confirmation_link
    visit(verification_link(CommonVars::EMAIL_SUBJECT))
    delete_email(CommonVars::EMAIL_SUBJECT)
  end
end
