class BasePage < SitePrism::Page

  element :header_logotype, '.header-logotype'

  def open_app
    visit CommonVars::WEB_APP_HOST
  end

  def refresh
    page.driver.browser.navigate.refresh
  end

  def home
    header_logotype.click
  end

  def accept_alert
    page.driver.browser.switch_to.alert.accept
  end
end
