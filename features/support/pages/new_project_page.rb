require_relative '../pages/base_page'
require_relative '../sections/user_top_section'

class NewProjectPage < BasePage

  element :project_name_input, '#project_name'
  element :email_recipient_input, '#project_recipient_email_list'
  element :cancel_button, 'a[href="/projects"]'
  element :create_button, 'input[type="submit"]'
  sections :top_section, UserTopSection, '.header-main'

  def type_project_name(project_name)
    project_name_input.set(project_name)
  end

  def type_email(email)
    email_recipient_input.set(email)
  end

  def goto_projects
    click_link 'Projects'
  end

  def create_project
    create_button.click
  end
end
