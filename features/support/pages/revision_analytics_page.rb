class RevisionAnalyticsPage < BasePage

  element :error_column, '.column-error span'
  element :amount_column, '.column-amount span'
  element :run_errors_column, '.run-path'
  element :copy_to_run_all_failures, '.input-group.run-failures-area'
  element :copy_to_clipboard_button, '.fa.fa-clipboard'
  element :search_input, '.dataTables_filter .form-control.input-sm'
  element :empty_table, '.dataTables_empty'

  def find_text(text)
   find('g', :text => text, :match => :first).text
  end

  def error_message_text
    error_message.text
  end

  def error_text
    error_column.text
  end

  def amount_text
    amount_column.text
  end

  def run_error_text
    run_errors_column.text
  end

  def copy_to_run_all_failures_text
    copy_to_run_all_failures.text
  end

  def click_copy_to_clipboard_button
    copy_to_clipboard_button.click
  end

  def set_search_input(text)
    search_input.set(text)
  end

  def clear_search_input
    search_input.native.clear
    search_input.send_keys :return
  end

  def empty_table_text
    empty_table.text
  end

  # must be in helper class
  def paste_copied_message
    `pbpaste`
  end

end
