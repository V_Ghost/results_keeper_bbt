class UserTopSection < SitePrism::Section
  element :current_user_link,  '.current-user-text'
  element :logout_link, '.logout-text'
  element :create_new_button,  '.create-new-project'
  element :projects_button, '.projects-link'
end
