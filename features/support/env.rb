require 'site_prism'
require 'rspec'
require 'capybara/cucumber'
require 'pry'
require 'selenium-webdriver'
require 'gmail'
require 'colorize'
require 'net/http'
require 'uri'
require_relative 'pages/base_page'
require_relative 'web_app'

def web_app
  @web_app ||= WebApp.new
end

Capybara.register_driver :chrome do |app|
  Capybara::Selenium::Driver.new(app, :browser => :chrome)
end

Capybara.default_driver = :chrome
Capybara.current_driver = :chrome

# Browser for Docker

# Capybara.register_driver :chrome do |app|
#   client = Selenium::WebDriver::Remote::Http::Default.new
#   client.read_timeout = 120
#   Capybara::Selenium::Driver.new(app, :browser => :chrome, http_client: client)
# end
# Capybara.default_max_wait_time = 60

# Capybara.javascript_driver = :chrome
# Capybara.default_driver = :chrome

# Capybara.register_driver :chrome do |app|
#   opts = {chromeOptions: {args: ['--ignore-certificate-errors']}}
#   browser_url = if ENV['SELENIUM_MODE'] == 'debug'
#                   puts ' === Running in DEBUG selenium mode ==='.yellow
#                   'selenium-chrome-debug'
#                 else
#                   puts ' === Running in Standard selenium mode. (Use "export SELENIUM_MODE=debug" to run in debug mode) ==='.green
#                   'selenium-chrome'
#                 end
#
#   capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(opts)
#   Capybara::Selenium::Driver.new(app, browser: :remote, url: "http://#{browser_url}:4444/wd/hub", desired_capabilities: capabilities)
# end

# docker_ip = %x(/sbin/ip route|awk '/default/ { print $3 }').strip
# Capybara.app_host = "http://#{docker_ip}:3010"
# Capybara.server_host = '0.0.0.0'
# Capybara.server_port = '3010'
