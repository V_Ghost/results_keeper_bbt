require 'pry'
require_relative 'api_client'

class ResultsGenerator

  class << self

    def send_revision(project_name, revision_name, secret_key)
      data = {revision: revision_name, project: project_name}
      response = ApiClient.send_json(data, 'revisions', secret_key)
      Scenario.reset_counter
      JSON.parse(response.body)['revision_id']
    end

    def send_revision_complete(project_name, revision_name, secret_key)
      data = {revision: revision_name, project: project_name, completed: 1}
      ApiClient.send_json(data, 'revisions', secret_key)
    end

    def send_test(scenario, revision_id, secret_key)
      scenario_steps = scenario.test_steps.map(&:name) unless ENV['NO_STEPS']
      scenario_error = scenario.exception.message if scenario.exception
      run_path = "cucumber #{scenario.location.file}:#{scenario.location.line}"
      data = {name: scenario.name,
              status: scenario.status,
              feature_name: scenario.feature.name,
              run_path: run_path,
              error: scenario_error,
              revision_id: revision_id,
              steps: scenario_steps
      }
      ApiClient.send_json(data, 'tests', secret_key)
    end
  end
end
