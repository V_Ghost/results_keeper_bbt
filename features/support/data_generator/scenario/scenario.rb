require_relative 'step'
require 'pry'
require 'ostruct'

class Scenario

  @@counter = 0

  attr_reader :name, :status

  def initialize(opts={})
    @@counter += 1
    @name = opts[:name] || "Test scenario - #{@@counter}"                #{Time.now} - #{rand(999)}"
    @steps_count = opts[:steps_count] || 5
    @status = opts[:status] || 'passed'
  end

  def self.reset_counter
    @@counter = 0
  end

  def test_steps
    res = []
    @steps_count.times do |t|
      res << Step.new("Test step #{t}")
    end
    res
  end

  def location
    params = { file: '/test/test/test.rb', line: 33 }
    OpenStruct.new(params)
  end

  def feature
    params = { name: 'The best feature ever' }
    OpenStruct.new(params)
  end

  def exception
    if @status == 'failed'
      params = { message: 'Failed message' }
      OpenStruct.new(params)
    end
  end
end


class ScenarioFactory

  def self.failed_scenario(n=false)
    res = []
    if n
      n.times do
        res << Scenario.new(status: 'failed')
      end
      res
    else
      Scenario.new
    end
  end

  def self.passed_scenario(n=false)
    res = []
    if n
      n.times do
        res << Scenario.new
      end
      res
    else
      Scenario.new
    end
  end
end
