require 'net/http'
require 'json'
require 'rest_client'

class ApiClient

  def self.send_json(body, path, secret_key)
    body['secret_key'] = secret_key
    @path = "/api/#{path}"
    @body = body.to_json
    request = Net::HTTP::Post.new(@path, initheader = {'Content-Type' => 'application/json'})
    request.body = @body
    Net::HTTP.new(CommonVars::API_SERVER_HOST, CommonVars::API_SERVER_PORT).start {|http| http.request(request)}
  end
end
