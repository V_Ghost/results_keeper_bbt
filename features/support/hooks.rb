require_relative 'google_drive_results'
@steps_count

After('@projects' ) do
  web_app.projects_page.remove_all_projects
end

After('@testing_api') do
  web_app.projects_page.remove_all_projects
end

After('@revision_analytics') do
  web_app.base_page.open_app
  web_app.projects_page.remove_all_projects
end

After('@revisions') do
  web_app.base_page.open_app
  web_app.projects_page.remove_all_projects
end

After('@tests') do
  web_app.base_page.open_app
  web_app.projects_page.remove_all_projects
end

After('@api') do
  RestClient.post("#{CommonVars::BOT_WITH_KEY}/chat_id", {'chat_id' => 426454156.to_s})
end

# Write in google docs results if update_status = true
if ENV['UPDATE_SHEETS'] == 'true'
  Before do |scenario|
    @count = 0
    GoogleDriveResults.write_to_document(scenario.feature.name)
    GoogleDriveResults.write_to_document(scenario.name)
  end

  AfterStep do
    @count += 1
  end

  After do |scenario|
    GoogleDriveResults.write_to_document(@count)
    GoogleDriveResults.write_to_document(scenario.status)
  end
end
