Then(/^page should include "([^"]*)" name and default email$/) do |name|
  expect(web_app.user_page.user_name_label.text).to include name
  expect(web_app.user_page.email_label.text).to include CommonVars::DEFAULT_USER_EMAIL
end