@user_info
  Feature: User info

    Scenario: User profile contains correct information
      When Results Keeper login page is open
      And types default email and password
      And clicks login button
      And user click on current user link
      Then page should include "Default User" name and default email
