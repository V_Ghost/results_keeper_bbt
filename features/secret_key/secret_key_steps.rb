
When(/^user click on current user link$/) do
  web_app.projects_page.top_section.map {|s| s.current_user_link.click}
end

Then(/^user copied secret key and tried to use it in tests$/) do
  secret_key = web_app.user_page.secret_key_field.text.to_s
  expect(ResultsGenerator.send_revision('Secret Key Test', 'Secret Key Test', secret_key)).to be, 'Revision must be passed'
  ResultsGenerator.send_revision_complete('Secret Key Test', 'Secret Key Test', secret_key)
end

Then(/^created new secret key and checked to use it$/) do
  secret_key_old = web_app.user_page.secret_key_field.text.to_s
  user_page = web_app.user_page
  user_page.genertate_new_secret_key
  secret_key_new = web_app.user_page.secret_key_field.text.to_s

  expect(ResultsGenerator.send_revision('Secret Key Test', 'Secret Key Test', secret_key_new)).to be, 'Revision must be passed'
  ResultsGenerator.send_revision_complete('Secret Key Test', 'Secret Key Test', secret_key_new)

  expect(ResultsGenerator.send_revision('Secret Key Test', 'Secret Key Test', secret_key_old)).to_not be, 'Revision must be in progress'
  ResultsGenerator.send_revision_complete('Secret Key Test', 'Secret Key Test', secret_key_old)
end
