@secret_key
Feature: Secret key

  Background: Open result keeper and login
    Given Results Keeper login page is open
    And logins with "SK_TEST_EMAIL" login and "SK_TEST_PASSWORD" password

  Scenario: Creating NEW user key
    And opens projects page
    When user click on current user link
    Then user copied secret key and tried to use it in tests
    Then created new secret key and checked to use it
