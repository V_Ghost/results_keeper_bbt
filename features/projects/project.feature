@projects
Feature: Projects

  Background:
    Given send revision to the API server:
      | project     | revision      | passed_count | failed_count |
      | TestProject | TestRevision1 | 3            | 1            |
      | TestProject | TestRevision2 | 3            | 1            |
      | TestProject | TestRevision3 | 3            | 0            |
      | TestProject | TestRevision4 | 3            | 1            |
    And Results Keeper login page is open
    And types default email and password
    And clicks login button

  Scenario: Add new project
    When user clicks create new project button
    And types "MyTestProject" project name
    And clicks create button
    Then the page should include "MyTestProject" text

  Scenario: Add new project and cancel
    When user clicks create new project button
    And types "MyTestProject" project name
    And clicks cancel button
    Then the page should not include "MyTestProject" text

  Scenario: Delete project
    When deletes "TestProject" project
    Then the page should not include "TestProject" text

  Scenario: Edit project
    When user clicks edit "TestProject" project button
    And types "UpdatedProject" project name
    And clicks update button
    Then the page should include "UpdatedProject" text

  Scenario: Edit project and cancel
    When user clicks edit "TestProject" project button
    And types "UpdatedProject" project name
    And clicks cancel button
    Then the page should not include "UpdatedProject" text

  Scenario: Revisions quantity on project's item counter
    When user clicks create new project button
    And types "MyTestProject" project name
    And clicks create button
    Then revisions counter shows "0" in "MyTestProject" project
    And sends revision to "MyTestProject" project with revision name "rev1"
    And refreshes page
    Then revisions counter shows "1" in "MyTestProject" project
    And sends revision to "MyTestProject" project with revision name "rev2"
    And refreshes page
    Then revisions counter shows "2" in "MyTestProject" project

 # Scenario: #TODO
 #   And clicks on project "TestProject" project item
