When(/^user clicks create new project button$/) do
  web_app.projects_page.click_new_project
end

When(/^user clicks edit "([^"]*)" project button$/) do |project_name|
  web_app.projects_page.edit_project(project_name)
end

When(/^types "([^"]*)" project name$/) do |project_name|
  web_app.new_project_page.type_project_name(project_name)
end

When(/^clicks create button$/) do
  web_app.new_project_page.create_project
end

When(/^clicks update button$/) do
  web_app.update_project_page.click_update
end

When(/^clicks cancel button$/) do
  web_app.update_project_page.click_cancel
end

When(/^clicks on project "([^"]*)" project item$/) do |project_name|
  web_app.projects_page.project_by_name(project_name).click
end

When(/^deletes "([^"]*)" project$/) do |project_name|
  web_app.projects_page.delete_project(project_name)
end

Then(/^the page should not include "([^"]*)" text$/) do |text|
  expect(page.text).not_to include(text)
end

When(/^types "([^"]*)" email$/) do |email|
  web_app.new_project_page.type_email(email)
end

Then(/^revisions counter shows "([^"]*)" in "([^"]*)" project$/) do |count, project_name|
  expect(web_app.projects_page.revisions_count(project_name)).to eq count
end

When (/^user clicks on "([^"]*)" project$/) do |project_name|
  web_app.projects_page.click_project(project_name)
end

When(/^sends revision to "([^"]*)" project with revision name "([^"]*)"$/) do |pn, rn|
  ResultsGenerator.send_revision(pn, rn, CommonVars::SECRET_KEY_PROJECTS)
end

When(/^send revision to the API server:$/) do |table|
  table.hashes.each do |row|
    revision_id = ResultsGenerator.send_revision(row['project'], row['revision'], CommonVars::SECRET_KEY_PROJECTS)
    if row['passed_count'] && row['failed_count']
      tests = []
      tests << ScenarioFactory.failed_scenario(row['failed_count'].to_i)
      tests << ScenarioFactory.passed_scenario(row['passed_count'].to_i)
      tests.flatten.each do |t|
        ResultsGenerator.send_test(t, revision_id, CommonVars::SECRET_KEY_PROJECTS)
      end
    end
  end
end
