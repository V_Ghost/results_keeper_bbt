Then(/^the page should include "([^"]*)" text$/) do |text|
  expect(page.text).to include(text)
end

When(/^Results Keeper login page is open$/) do
  web_app.base_page.open_app
end

When(/^refreshes page$/) do
  web_app.base_page.refresh
end

Given(/^opens projects page$/) do
  web_app.base_page.header_logotype.click
end

And (/^types default email and password$/) do
  web_app.login_page.type_creds(CommonVars::DEFAULT_USER_EMAIL, CommonVars::DEFAULT_USER_PASSWORD)
end

