When(/^types "([^"]*)" email and "([^"]*)" password$/) do |email, password|
  web_app.login_page.type_creds(email, password)
end

When(/^clicks login button$/) do
  web_app.login_page.click_login
end

When(/^logins with "([^"]*)" login and "([^"]*)" password$/) do |login, password|
  login_page = LoginPage.new
  login_page.type_creds(CommonVars.const_get(login), CommonVars.const_get(password))
  login_page.click_login
end

