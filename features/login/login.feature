@login
Feature: User Login

  Scenario: Login with right creds
    When Results Keeper login page is open
    And types default email and password
    And clicks login button
    Then the page should include "Default User" text

  Scenario: Login with incorrect credentials
    When Results Keeper login page is open
    And types "tymofii.kritsak@gmail.com" email and "QWERTY12345" password
    Then the page should include "Don’t have an Account?" text

