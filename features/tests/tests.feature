@tests
Feature: Tests

  Background:
    Given send revision to the API server:
      | project     | revision      | passed_count | failed_count |
      | TestProject | TestRevision1 | 3            | 1            |
    And Results Keeper login page is open
    And types default email and password
    And clicks login button
    And user clicks on "TestProject" project
    And clicks on "TestRevision1" revision

  Scenario: Check that Name and Revision is valid
    When clicks on "Test scenario - 2" test
    Then the page should include "Test scenario - 2" text
    And the page should include "TestRevision1" text

  Scenario: Check test Location text
    When clicks on "Test scenario - 2" test
    Then test Location should be "cucumber /test/test/test.rb:33"

  Scenario: Check test Passed Status
    When clicks on "Test scenario - 2" test
    Then the page should include "PASSED" text

  Scenario: Check test Failed Status
    When clicks on "Test scenario - 1" test
    Then the page should include "FAILED" text

  Scenario: Check that Test Steps displays correctly
    When clicks on "Test scenario - 2" test
    Then the page should include "Test step 0" text
    And the page should include "Test step 1" text
    And the page should include "Test step 2" text
    And the page should include "Test step 3" text
    And the page should include "Test step 4" text

  Scenario: Check Run Amount value for passed test
    When clicks on "Test scenario - 2" test
    Then run amount Passed text should contain 1 passed

  Scenario: Check Run Amount value for failed test
    When clicks on "Test scenario - 1" test
    Then run amount Failed text should contain 1 failed

  Scenario: Check Copy Link to Clipboard button works
    When clicks on "Test scenario - 2" test
    And clicks on copy button
    Then copied message should be "cucumber /test/test/test.rb:33"





  # TODO:
 # Duration, Location_button, Previous status, Last Success On, Screenshot_url, Screenshot

#  @duration
#  Scenario: Check test Duration
#    When user clicks on "TestProject" project
#    And waits 5 seconds
#    And sends revision complete to "TestRevision1"
#    And refreshes page
#    And clicks on "TestRevision1" revision
#    And clicks on "Test scenario - 2" test
#