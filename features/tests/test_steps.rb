And(/^clicks on "([^"]*)" test$/) do |test_name|
  web_app.current_revision_page.click_current_test(test_name)
end

Then (/^the test "([^"]*)" should be "([^"]*)" on the Test Page$/) do |key, value|
  web_app.test_page.find_value(key)
end

Then (/^test Location should be "([^"]*)"$/) do |text|
  expect(web_app.test_page.location_text).to eq text
end

Then (/^run amount Failed text should contain (\d+) failed$/) do |failed_value|
  expect(web_app.test_page.run_amount_failed_text).to include failed_value
end

Then (/^run amount Passed text should contain (\d+) passed/) do |passed_value|
  expect(web_app.test_page.run_amount_passed_text).to include passed_value
end