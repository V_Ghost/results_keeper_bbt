@registration
Feature: Register a new user

  Background: visit Results Keeper
    Given Results Keeper login page is open

  Scenario: user registers a new user
    When user clicks on Create yours now link
    And fill sign up form with "Test" first name and "User" last name
    And clicks submit button
    And navigates to email and confirms it
    And Results Keeper login page is open
    And user types generated email and password
    And clicks login button
    Then the page should include "Test User" text
