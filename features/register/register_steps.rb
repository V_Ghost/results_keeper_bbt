When(/^user clicks on Create yours now link$/) do
  web_app.login_page.click_register
end

When(/^fill sign up form with "([^"]*)" first name and "([^"]*)" last name$/) do |first_name, last_name|
  web_app.register_page.fill_register_data(first_name,
                                           last_name,
                                           CommonVars::RANDOM_USER_EMAIL,
                                           CommonVars::DEFAULT_USER_PASSWORD)
end

When(/^clicks submit button$/) do
  web_app.register_page.click_submit_button
end

When(/^navigates to email and confirms it$/) do
  web_app.gmail_page.visit_confirmation_link
end

When(/^user types generated email and password$/) do
  web_app.login_page.type_creds(CommonVars::RANDOM_USER_EMAIL, CommonVars::DEFAULT_USER_PASSWORD)
end
