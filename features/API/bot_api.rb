Then(/^I send 'get' request "([^"]*)"$/) do |body|
  @response = RestClient.get(CommonVars::BOT_WITH_KEY + body)
  @bot_response = JSON.parse(@response)
end

Then(/^I should get the following array of hashes as a response:$/) do |table|
  expect(web_app.api_methods_page.all_hashes_present?(@bot_response, table.hashes)).to eq true
end

Then(/^I send 'post' request "([^"]*)" with "([^"]*)"$/) do |body, data|
  response = RestClient.post("#{CommonVars::BOT_WITH_KEY}#{body}", {body => data.to_s})
  expect(response.code.to_s).to include '200'
end

Then(/^I should get "([^"]*)" "([^"]*)"$/) do |id, resp|
  expect(@bot_response[id].to_s).to eq resp
end

Then(/^I should get the following hash as a response:$/) do |table|
  table.hashes.each do |hash|
    expect(web_app.api_methods_page.hash_values_to_string(@bot_response)).to include hash
  end
end

When(/^I send 'get' request to "([^"]*)" with not valid token$/) do |body|
  begin
    @invalid_response = JSON.parse(RestClient.get(CommonVars::UNKNOWN_BOT_URL + body))
  rescue
  end
end

And(/^I should get status error with "([^"]*)" code$/) do |status_code|
  begin
    @invalid_response
  rescue => e
    expect(e.to_s).to include status_code
  end
end

Then(/^status code should be "([^"]*)"$/) do |status_code|
  expect(@response.code.to_s).to include status_code
end

Then(/^response key values should be equal to (\d+)$/) do |key_count|
  expect(@bot_response.keys.count).to eq key_count.to_i
end

Then(/^response key array values should be equal to (\d+)$/) do |key_count|
  expect(@bot_response.first.keys.count).to eq key_count.to_i
end