@api
Feature: Bot_API tests

  Scenario: Check if user exists
    When I send 'get' request "/check_user"
    Then status code should be "200"
    And response key values should be equal to 1
    And I should get "response" "true"

  Scenario: Check if user not exist
    When I send 'get' request to "/check_user" with not valid token
    Then I should get status error with "404" code

  Scenario: Test chat_id change via post request
    When I send 'get' request "user"
    Then status code should be "200"
    And response key values should be equal to 9
    And I should get the following hash as a response:
      | id  | first_name | last_name | email                       | chat_id   |
      | 110 | Bot        | Test      | v.ghost.test+1111@gmail.com | 426454156 |
    When I send 'post' request "chat_id" with "23423423"
    And I send 'get' request "user"
    Then status code should be "200"
    And response key values should be equal to 9
    And I should get "chat_id" "23423423"

  Scenario: Test chat_id change to nil
    When I send 'post' request "chat_id" with "nil"
    And I send 'get' request "user"
    Then status code should be "200"
    And response key values should be equal to 9
    And I should get "chat_id" "nil"

  Scenario: Get projects related to user
    When I send 'get' request "projects"
    Then status code should be "200"
    And response key array values should be equal to 4
    And I should get the following array of hashes as a response:
      | id   | name             | revisions_amount | failed_revisions_amount |
      | 1376 | Results Keeper   | 18               | 6                       |
      | 1463 | My First Project | 1                | 0                       |
      | 1464 | Results Keeper1  | 3                | 0                       |

  Scenario: Get projects to invalid user
    When I send 'get' request to "projects" with not valid token
    Then I should get status error with "404" code

  Scenario: Get exact project related to user
    When I send 'get' request "projects/1376"
    Then status code should be "200"
    And response key values should be equal to 4
    And I should get the following hash as a response:
      | id   | name           | revisions_amount | failed_revisions_amount |
      | 1376 | Results Keeper | 18               | 6                       |

  Scenario: Get current project to invalid user
    When I send 'get' request to "projects/1376" with not valid token
    Then I should get status error with "404" code

  Scenario: Get revisions related to project
    When I send 'get' request "projects/1376/revisions"
    Then status code should be "200"
    And response key array values should be equal to 10
    And I should get the following array of hashes as a response:
      | id   | name                                 | completed | status      | test_amount | failed_test_amount | duration | project_id |
      | 3028 | Result for 2017-09-12 17:02:00 +0300 | 1         | failed      | 8           | 2                  | 00:00:10 | 1376       |
      | 2822 | Result for 2017-09-06 16:19:58 +0300 | 0         | in progress | 3           | 0                  | 00:00:10 | 1376       |

  Scenario: Get revisions to invalid user
    When I send 'get' request to "projects/1376/revisions" with not valid token
    Then I should get status error with "404" code

  Scenario: Get current revision related to project
    When I send 'get' request "projects/1376/revisions/2797"
    Then status code should be "200"
    And response key values should be equal to 10
    And I should get the following hash as a response:
      | id   | name                                 | completed | status | test_amount | failed_test_amount | project_id |
      | 2797 | Result for 2017-09-06 16:17:59 +0300 |           | failed | 10          | 2                  | 1376       |

  Scenario: Get current revision to invalid user
    When I send 'get' request to "projects/1376/revisions/2797" with not valid token
    Then I should get status error with "404" code

  Scenario: Get list of tests from  exact revision
    When I send 'get' request "projects/1376/revisions/2903/tests"
    Then status code should be "200"
    And response key array values should be equal to 16
    And I should get the following array of hashes as a response:
      | id    | name                   | duration     | status | previous_status | error | run_path                                | run_amount | failed_amount | last_success | screenshot_url | feature_name | project_id | revision_id | revision_name                        |
      | 10374 | Login with right creds | 4.87 seconds | passed | passed          |       | cucumber features/login/login.feature:4 | 5          | 0             | 09/12/2017   |                |              | 1376       | 2903        | Result for 2017-09-06 16:27:01 +0300 |


  Scenario: Get tests to invalid user
    When I send 'get' request to "projects/1376/revisions/2903/tests" with not valid token
    Then I should get status error with "404" code


  Scenario: Get exact test from current revision
    When I send 'get' request "projects/1376/revisions/2903/tests/10472"
    Then status code should be "200"
    And response key values should be equal to 16
    Then I should get the following hash as a response:
      | id    | name                      | status | duration     | run_path                                      | revision_id | project_id | error                                                                                                                                                                                                 |
      | 10472 | user registers a new user | failed | 8.85 seconds | cucumber features/register/register.feature:7 | 2903        | 1376       | expected "Remember me Don’t have an Account?Create yours now.Forgot your password?Didn't receive confirmation instructions? RESULTS KEEPER \| COPYRIGHT © ALL RIGHTS RESERVED" to include "Test User" |

  Scenario: Get current test to invalid user
    When I send 'get' request to "projects/1376/revisions/2903/tests/10472" with not valid token
    Then I should get status error with "404" code