Then(/^Amount value should be "([^"]*)"$/) do |amount_value|
  expect(web_app.revisions_page.revision_amount_text).to eq amount_value
end

Then(/^Passed value should be "([^"]*)"$/) do |passed_value|
  expect(web_app.revisions_page.revision_passed_text).to eq passed_value
end

Then(/^Failed value should be "([^"]*)"$/) do |failed_value|
  expect(web_app.revisions_page.revision_failed_text).to eq failed_value
end

When(/^waits (\d+) seconds$/) do |delay|
  sleep CommonVars::DEFAULT_DELAY * delay.to_i
end

When(/^sends revision complete to "([^"]*)"$/) do |revision_name|
  ResultsGenerator.send_revision_complete('TestProject', revision_name, CommonVars::SECRET_KEY_PROJECTS)
end

Then(/^"([^"]*)" revision Duration should be between (\d+)\-(\d+) seconds$/) do |revision_name, min_value, max_value|
  expect(web_app.revisions_page.duration_in_seconds(revision_name))
      .to be_between(min_value.to_i, max_value.to_i).inclusive
end

Then(/^"([^"]*)" revision status should be "([^"]*)"$/) do |revision_name, status|
  expect(web_app.revisions_page.status(revision_name)).to eq status
end

And(/^sends test to "([^"]*)" revision in "([^"]*)" project/) do |revision_name, project_name|
  revision_id = ResultsGenerator.send_revision(project_name, revision_name, CommonVars::SECRET_KEY_PROJECTS)
  test = ScenarioFactory.passed_scenario
  ResultsGenerator.send_test(test, revision_id,CommonVars::SECRET_KEY_PROJECTS)
end

Then(/^"([^"]*)" revision Amount value should be "([^"]*)"$/) do |revision_name, amount_value|
  expect(web_app.revisions_page.tests_amount(revision_name)).to eq amount_value
end

Then (/^started at date for "([^"]*)" revision should be Today date$/) do |revision_name|
  expect(web_app.revisions_page.started_at(revision_name).gsub(/\s.+/, '')).to eq Time.now.to_s.gsub(/\s.+/, '')
end

And (/^clicks delete "([^"]*)" revision$/) do |revision_name|
  web_app.revisions_page.click_delete_revision(revision_name)
end

And (/^clicks finish "([^"]*)" revision$/) do |revision_name|
  web_app.revisions_page.click_finish_revision(revision_name)
end

Then (/^failed amount value for "([^"]*)" should be (\d+)$/) do |revision_name, failed_amount|
  expect(web_app.revisions_page.failed_amount(revision_name)).to eq failed_amount
end
