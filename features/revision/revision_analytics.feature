@revision_analytics
Feature: Revisions

  Background:
    Given send revision to the API server:
      | project     | revision      | passed_count | failed_count |
      | TestProject | TestRevision2 | 4            | 0            |
      | TestProject | TestRevision4 | 3            | 1            |
    And Results Keeper login page is open
    And types default email and password
    And clicks login button

  Scenario: Check revision Analytics chart tab
    When user clicks on "TestProject" project
    And clicks on "TestRevision4" revision
    And clicks on Revision Analytics
    Then count of passed tests should be "75%"
    And count of failed tests should be "25%"
    And percent of passed tests should be "75%"
    And percent of failed tests should be "25%"

  Scenario: Check revision Analytics Error trend tab
    When user clicks on "TestProject" project
    And clicks on "TestRevision4" revision
    And clicks on Revision Analytics
    And clicks on Error trend tab
    Then the Error should be "Failed message"
    And amount should be "1"
    And run errors should be "cucumber /test/test/test.rb:33"

  Scenario: Check revision Analytics Return All Failures tab
    When user clicks on "TestProject" project
    And clicks on "TestRevision4" revision
    And clicks on Revision Analytics
    And clicks on Return All Failurtes tab
    Then copy to Run All Failures text should be "cucumber /test/test/test.rb:33"

  Scenario: Check copy button on Analytics Error trend tab
    When user clicks on "TestProject" project
    And clicks on "TestRevision4" revision
    And clicks on Revision Analytics
    And clicks on Error trend tab
    And clicks on copy button
    Then copied message should be "cucumber /test/test/test.rb:33"

  Scenario: Check search on Revision Analytics Error Trend tab
    When user clicks on "TestProject" project
    And clicks on "TestRevision4" revision
    And clicks on Revision Analytics
    And clicks on Error trend tab
    And types "Failed message" in search fileld
    Then the Error should be "Failed message"
    And clear search field

  Scenario: Check search on Revision Analytics Error Trend tab (No results)
    When user clicks on "TestProject" project
    And clicks on "TestRevision4" revision
    And clicks on Revision Analytics
    And clicks on Error trend tab
    And types "QWERTY123123123" in search fileld
    Then the table should show "No matching records found"
    And clear search field
