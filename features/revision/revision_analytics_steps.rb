When(/^clicks on "([^"]*)" revision$/) do |revision_name|
  web_app.current_project_page.click_current_revision(revision_name)
end

When(/^clicks on Revision Analytics$/) do
  web_app.current_project_page.click_revision_analytics
end

Then(/^count of passed tests should be "([^"]*)"$/) do |passed|
  web_app.revision_analytics_page.find_text(passed)
end

Then(/^count of failed tests should be "([^"]*)"$/) do |failed|
  web_app.revision_analytics_page.find_text(failed)
end

Then(/^percent of passed tests should be "([^"]*)"$/) do |passed|
  web_app.revision_analytics_page.find_text(passed)
end

Then(/^percent of failed tests should be "([^"]*)"$/) do |failed|
  web_app.revision_analytics_page.find_text(failed)
end

And(/^clicks on Error trend tab$/) do
  web_app.current_project_page.click_error_trend_tab
end

Then(/^the Error should be "([^"]*)"$/) do |error_message|
  expect(web_app.revision_analytics_page.error_text).to eq error_message
end

Then(/^amount should be "([^"]*)"$/) do |amount|
  expect(web_app.revision_analytics_page.amount_text).to eq amount
end

Then(/^run errors should be "([^"]*)"$/) do |run_error_message|
  expect(web_app.revision_analytics_page.run_error_text).to eq run_error_message
end

And(/^clicks on Return All Failurtes tab$/) do
  web_app.current_project_page.click_return_all_failures_tab
end

Then(/^copy to Run All Failures text should be "([^"]*)"$/) do |run_failures_message|
  expect(web_app.revision_analytics_page.copy_to_run_all_failures_text).to eq run_failures_message
end

And(/^clicks on copy button$/) do
  web_app.revision_analytics_page.click_copy_to_clipboard_button
end
Then (/^copied message should be "([^"]*)"$/) do |message|
  expect(web_app.revision_analytics_page.paste_copied_message).to eq message
end

And(/^types "([^"]*)" in search fileld$/) do|text|
  web_app.revision_analytics_page.set_search_input(text)
end

Then(/^the table should show "([^"]*)"$/) do |empty_message_text|
  expect(web_app.revision_analytics_page.empty_table_text).to eq empty_message_text
end

And(/^clear search field$/) do
  web_app.revision_analytics_page.clear_search_input
end