@revisions
Feature: Revisions

  Background:

    Given send revision to the API server:
      | project     | revision      | passed_count | failed_count |
      | TestProject | TestRevision4 | 3            | 1            |
      | TestProject | TestRev1      | 1            | 0            |
    And Results Keeper login page is open
    And types default email and password
    And clicks login button
    And user clicks on "TestProject" project

  Scenario: Check Amount/Passed/Failed tests on Revision Page
    When clicks on "TestRevision4" revision
    Then Amount value should be "4"
    And Passed value should be "3"
    And Failed value should be "1"

  Scenario: Check that revision duration displays correctly
    When waits 10 seconds
    And sends revision complete to "TestRevision4"
    And refreshes page
    Then "TestRevision4" revision Duration should be between 10-20 seconds

  Scenario: Check revision Status (failed)
    Then "TestRevision4" revision status should be "in-progress"
    And sends revision complete to "TestRevision4"
    And refreshes page
    Then "TestRevision4" revision status should be "failed"

  Scenario: Check revision Status (passed)
    Then "TestRev1" revision status should be "in-progress"
    And sends revision complete to "TestRev1"
    And refreshes page
    Then "TestRev1" revision status should be "passed"

  Scenario: Check revision Test Amount
    Then "TestRevision4" revision Amount value should be "4"
    And sends test to "TestRevision4" revision in "TestProject" project
    And refreshes page
    Then "TestRevision4" revision Amount value should be "5"
    And sends test to "TestRevision4" revision in "TestProject" project
    And refreshes page
    Then "TestRevision4" revision Amount value should be "6"

  Scenario: Check that date shows correctly
    Then started at date for "TestRevision4" revision should be Today date

  Scenario: Verify that delete revision button works
    When clicks delete "TestRev1" revision
    Then the page should not include "TestRev1" text

  Scenario: Verify that finish revision button works
    When clicks finish "TestRev1" revision
    Then "TestRev1" revision status should be "passed"
    And clicks finish "TestRevision4" revision
    Then "TestRevision4" revision status should be "failed"

  Scenario: Check Failed Amount column
    And clicks finish "TestRevision4" revision
    Then failed amount value for "TestRevision4" should be 1

    @test111
  Scenario: Check revision search (no results)
    When types "QWERTY123123123" in search fileld
    Then the table should show "No matching records found"
    And clear search field

  Scenario: Check revision search (by name)
    When types "TestRevision4" in search fileld
    Then the page should include "TestRevision4" text
    And the page should not include "TestRev1" text
    And clear search field
