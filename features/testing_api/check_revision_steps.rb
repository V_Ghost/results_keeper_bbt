And(/^clicks on "([^"]*)" project$/) do |project_name|
  web_app.projects_page.project_by_name(project_name).click
end

And(/^revision "([^"]*)" is present in the table$/) do |revision_name|
  expect(web_app.revisions_page.revision_exists?(revision_name)).to eq(true)
end

And(/^revision "([^"]*)" has status "([^"]*)"$/) do |revision_name, revision_status|
  expect(web_app.revisions_page.row(revision_name)[:status]).to eq(revision_status)
end

And(/^revision "([^"]*)" in "([^"]*)" project complete$/) do |revision_name, project_name|
  ResultsGenerator.send_revision_complete(project_name, revision_name, CommonVars::TEST_API_SECRET_KEY)
end

And(/^wait some time time$/) do
  sleep(CommonVars::DEFAULT_DELAY)
end
