@testing_api
Feature: Create new project

  Background: Copy secret key
    When Results Keeper login page is open
    And logins with "TEST_API_EMAIL" login and "TEST_API_PASSWORD" password

  Scenario: Creating new project
    And user creates "Test1" project name and "Rev1" revision name
    And opens projects page
    Then project "Test1" with revision "Rev1" is present in list of the projects
