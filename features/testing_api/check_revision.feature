@testing_api
Feature: Revision API

  Background: Copy secret key
    Given Results Keeper login page is open
    And logins with "TEST_API_EMAIL" login and "TEST_API_PASSWORD" password

  Scenario: Adding revision in project
    And user creates "Test12" project name and "Test21" revision name
    And opens projects page
    And refreshes page
    And clicks on "Test12" project
    Then revision "Test21" is present in the table
    And revision "Test21" has status "in-progress"
    Then revision "Test21" in "Test12" project complete
    And refreshes page
    Then revision "Test21" has status "passed"
