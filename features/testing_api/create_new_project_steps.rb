require_relative '../support/data_generator/results_generator'
require_relative '../support/data_generator/scenario/scenario'

Then(/^user copy secret key and save it in class variable$/) do
  UserCreds.secret_key(web_app.user_page.secret_key_field.text.to_s)
end

When(/^user creates "([^"]*)" project name and "([^"]*)" revision name$/) do |project_name, revision_name|
  ResultsGenerator.send_revision(project_name, revision_name, CommonVars::TEST_API_SECRET_KEY)
end

Then(/^project "([^"]*)" with revision "([^"]*)" is present in list of the projects$/) do |project_name, revision_name|
  expect(web_app.projects_page.project_exists?(project_name)).to eq(true)
  ResultsGenerator.send_revision_complete(project_name, revision_name, CommonVars::TEST_API_SECRET_KEY)
end
