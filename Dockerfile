FROM ruby:2.3.3

WORKDIR /tmp
ADD ./Gemfile Gemfile

RUN bundle install

ENV APP_ROOT /workspace
RUN mkdir -p $APP_ROOT
WORKDIR $APP_ROOT
COPY . $APP_ROOT
